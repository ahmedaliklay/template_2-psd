/* 
    created By: Ahmed Ali Klay
    Email :aa9141179@gmail.com;
    phone:0112 6350 859
*/
$(function(){
    var windowheight = $(window).height(),
        upperheight  = $('.upper_bar').innerHeight(),
        navHeighht   = $('.navbar').innerHeight();

        $('.slider,.carousel-item').height(windowheight - (upperheight + navHeighht));
        /////////////////////////
        $('.featured_work ul li').on('click',function(){
            $(this).addClass('active').siblings().removeClass('active');
                //console.log($(this).data('class'));
             if($(this).data('class') === 'all'){
                 $('.shuffile_images .col-md').css('opacity',1);
                
             }else{
                $('.shuffile_images .col-md').css('opacity','.3')

                $($(this).data('class')).parent().css('opacity',1);
             }
        });
        ////////////////
});